package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class Controller{

   // ObservableList<Integer> langs = FXCollections.observableArrayList();
    @FXML
    private ListView<Integer> listView= new ListView<>();

    private Socket socket = new Socket();

    private byte[] bytes;

    @FXML
    void action() throws IOException {
        socket.setCommandStart();
        bytes = socket.getDataSocket();
        String str = new String(bytes, StandardCharsets.UTF_8);
        for (String retval : str.split(",")) {
            try {
                listView.getItems().add(Integer.valueOf(retval));
            }catch (Exception e){}
        }
    }


    @FXML
    void save(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"));

        fileChooser.setInitialFileName("untitled");

        File file = fileChooser.showSaveDialog(null);
        System.out.println(file.getAbsolutePath());
        Path path = Paths.get(file.getAbsolutePath());
        try {
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
