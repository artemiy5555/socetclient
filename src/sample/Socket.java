package sample;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Socket {

    private SocketChannel channel;
    private SocketAddress socketAddress;

    private ByteBuffer byteBuffer;

    public void setCommandStart() throws IOException {
        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress("localhost",9000);
        channel.connect(socketAddress);

        byteBuffer = ByteBuffer.allocate(1024);

        ByteBuffer buf = ByteBuffer.wrap("HelloFromFX".getBytes(StandardCharsets.UTF_8));

        while (true){
            try {
                if (!(channel.write(buf) > 0)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("We have gone");
        }

    }

    public byte[] getDataSocket() {

        byte[] bytes = new byte[1000];
        try {
            channel.read(byteBuffer);
            byteBuffer.flip();
            bytes =byteBuffer.array();
//            Path path = Paths.get("note.csv");
//            Files.write(path, byteBuffer.array());

            byteBuffer.clear();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return bytes;
    }
}
